## Follow the steps specified for your machine an OS:
- [Windows](#windows)

## Windows

## Install GIT & Eclipse

- Download & Install [GIT], use defaut setings when install it (https://git-scm.com/downloads/index.html)
- Download & Install latest stable [JRE] (https://www.oracle.com/technetwork/java/javase/downloads)
- Download [Eclipse] (https://www.eclipse.org/downloads/)
- When installing Eclipse chose Eclipse IDE for Java Developers  
![](/src/resource/readme/eclipseide.png)  
- Open Help->Eclipse Marketplace and search for JavaFX  
![](/src/resource/readme/menu-ecplise.png)  
- install e(fx)clipse  
![](/src/resource/readme/e(fx)clipse.png)  

## Connection with gitlab

- Go to %HOMEDRIVE%%HOMEPATH%\.ssh and see if you have ssh.pub file there
- IF NOT open git bash and generate your own ssh public key with the folowing command:
```
$ ssh-keygen -t rsa
```  
![](/src/resource/readme/ssh-keygen.png)  
- go back to %HOMEDRIVE%%HOMEPATH%\.ssh and open in a notepad ssh.pub file
- go to your gitlab acount and go to settings menu  
![](/src/resource/readme/settings.png)  
- and on the menu in the left select SSH Keys  
![](/src/resource/readme/ssh.png)  
- there paste your ssh public key from ssh.pub (be careful to to copy the whole key, including first line)
- and gave it a title to recognise the device is associated with

## Get clone remote repository and open it in eclipse
- open git bash and go to the folder you want to drop in the project (Example: ProjectsFolder), there insert folowing command:
```
git clone git@gitlab.com:irulan/multicon.git
```

If clone via ssh is not working you can try clone it via https with
```
git clone https://gitlab.com/irulan/multicon.git
```

## Get dependencies
- Jssc Jar [2.8.0] (https://jar-download.com/artifacts/org.scream3r/jssc/2.8.0/source-code/)
- Open Project Properties -> Java Build Path -> Libraries -> Add External JARs
- Download [JDBC], select platform independent (https://dev.mysql.com/downloads/connector/j/)
- etract jar to folder and add as External JAR

## Scene Builder
- download latest [JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
- download and install [Scene Builder](https://gluonhq.com/products/scene-builder/)
- to connect with eclipse open the Window->Preferences and navigate to JavaFX  
![](/src/resource/readme/preferences.png)  
- there you need to add the complete path to your Scene Builder executable  

