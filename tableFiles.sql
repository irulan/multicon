CREATE TABLE userFile (
    fileId int PRIMARY KEY AUTO_INCREMENT,
    userName varchar(100) NOT NULL,
    fileName varchar(115) NOT NULL,
    fileDate date NOT NULL,
    fileTime time NOT NULL,
    FOREIGN KEY (userName) REFERENCES users(userName)
);