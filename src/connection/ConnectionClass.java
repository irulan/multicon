package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionClass {
	
	protected static Connection connection = null;
	
	private static final String CLASS_FORNAME = "com.mysql.cj.jdbc.Driver";
    private static final String DATABASE_LOCATION = "jdbc:mysql://techdex.ro:3306/";//  "jdbc:mysql://localhost:3306/";
    private static final String DATABASE_NAME = "techd_multicon";
    private static final String USERNAME = "techd_main";//"root";
    private static final String PASSWD = "war2015.";//"";
	
	public Connection getConnection() {
		String dbName = DATABASE_NAME;
		String username= USERNAME;
		String password=PASSWD;
		
		try {
			Class.forName(CLASS_FORNAME);
			connection = DriverManager.getConnection( DATABASE_LOCATION + dbName + "?user=" + username + "&password=" + password);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return connection;
	}
	
	public void closeConnection() throws SQLException {
		connection.close();
	}

}

