package connection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

import connection.ConnectionClass;

public class TestConnection {
	
	public static void assignFileToUser(String fileName, String userName, Date date, Time time) {
		ConnectionClass connectionclass = new ConnectionClass();
		Connection connection = connectionclass.getConnection();
		String query = "INSERT INTO userFile (userName, fileName, fileDate, fileTime) VALUES (?, ?, ?, ?)";
		try {
			PreparedStatement preptState = connection.prepareStatement(query);
			preptState.setString(1, userName);
			preptState.setString(2, fileName);
			preptState.setDate(3, date);
			preptState.setTime(4, time);

			preptState.execute();
			
			connection.close();
		}catch(Exception e) {
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}				
	}
	
	public int getUserPermisions(String username) {
		ConnectionClass connectionclass = new ConnectionClass();
		Connection connection = connectionclass.getConnection();
		
		String query = "SELECT userType FROM users WHERE userName = ? ";
		try {
			PreparedStatement preptState = connection.prepareStatement(query);
			preptState.setString(1, username);
			ResultSet userResults = preptState.executeQuery();
			while(userResults.next()) {
				String userType = userResults.getString("userType");
				Integer ut = Integer.parseInt(userType);
				return ut;
			}
		} catch(Exception e) {
			
		}
		return -1;
	}
	
	public int getUserId(String username) {
		ConnectionClass connectionclass = new ConnectionClass();
		Connection connection = connectionclass.getConnection();
		
		String query = "SELECT userId FROM users WHERE userName = ? ";
		try {
			PreparedStatement preptState = connection.prepareStatement(query);
			preptState.setString(1, username);
			ResultSet userResults = preptState.executeQuery();
			while(userResults.next()) {
				String userType = userResults.getString("userType");
				Integer ut = Integer.parseInt(userType);
				return ut;
			}
		} catch(Exception e) {
			
		}
		return -1;
	}
	
	public boolean testLogIn(String username, String passwd) {
		
		ConnectionClass connectionclass = new ConnectionClass();
		Connection connection = connectionclass.getConnection();
		
		String query = "SELECT hasAccess FROM users WHERE userName = ? ";
		try {
			PreparedStatement preptState = connection.prepareStatement(query);
			preptState.setString(1, username);
			ResultSet userResults = preptState.executeQuery();
			while(userResults.next()) {
				String hasAccess = userResults.getString("hasAccess");
				Integer ha = Integer.parseInt(hasAccess);
				if (ha == 1)
				{
					query = "SELECT passwd FROM logIn WHERE userId = ?";
					preptState = connection.prepareStatement( query );
					preptState.setString(1, username);
					ResultSet passResults = preptState.executeQuery();
					while(passResults.next()) {
						String pass = passResults.getString("passwd");
						String md5Pass = Encryption.MD5(passwd);
												
						if(pass.equals(md5Pass))
						{
							connectionclass.closeConnection();
							return true;
						}
					}	
				}
				else
					connectionclass.closeConnection();
					return false;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		try {
			connectionclass.closeConnection();
		} catch(SQLException e) {
			
		}
		return false;
	}

}
