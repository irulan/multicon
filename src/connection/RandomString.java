package connection;
import java.util.UUID;

public class RandomString {
	
	String uuid;

	public RandomString() {
		this.uuid = this.generate();
		this.uuid.replace("-", "");
	}
	
	private String generate() {
		return UUID.randomUUID().toString();
	}

}
