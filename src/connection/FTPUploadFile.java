package connection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
 
/**
 * A program that demonstrates how to upload files from local computer
 * to a remote FTP server using Apache Commons Net API.
 * @author www.codejava.net
 */

public class FTPUploadFile {
	
	private static final String server = "ftp.techdex.ro";
	private static final String user = "multidex@techdex.ro";
	private static final String pass = "o9!Xvxe+o&Qd";
	private static final int port = 21;

	public static void getFTPConnection(String localPath, String remotePath) {    
    
		FTPClient ftpClient = new FTPClient();

    
		try {

			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();

			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			// APPROACH #1: uploads first file using an InputStream
			File firstLocalFile = new File(localPath); // File firstLocalFile = new File("D:/Test/Projects.zip");

			String firstRemoteFile = remotePath; // String firstRemoteFile = "Projects.zip";
			InputStream inputStream = new FileInputStream(firstLocalFile);

			System.out.println("Start uploading file");
			boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
			inputStream.close();
			if (done) {
				System.out.println("The file is uploaded successfully.");
			}

			// APPROACH #2: uploads second file using an OutputStream
/*			File secondLocalFile = new File(localPath);
			String secondRemoteFile = remotePath;
			InputStream inputStream = new FileInputStream(secondLocalFile);

			System.out.println("Start uploading file");
			OutputStream outputStream = ftpClient.storeFileStream(secondRemoteFile);
			System.out.println("1.");
			byte[] bytesIn = new byte[4096];
			System.out.println("2.");
			int read = 0;
			System.out.println("3.");
			while ((read = inputStream.read(bytesIn)) != -1) {
				System.out.println("4.");
				outputStream.write(bytesIn, 0, read);
			}
			System.out.println("5.");
			inputStream.close();
			System.out.println("6.");
			outputStream.close();
			System.out.println("7.");

			boolean completed = ftpClient.completePendingCommand();
			if (completed) {
				System.out.println("File is uploaded successfully.");
			}
*/
		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

}
