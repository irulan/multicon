package application;

//import connection.SerialConnection;

import java.io.IOException;

import connection.TestConnection;
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
//import javafx.stage.Stage;

public class MenuFX {
	@FXML
	private Label labelUsername;
	
	@FXML
	private GridPane griPaneMeniu;
	
	@FXML
	private ComboBox<String> portComboBox;
	
	@FXML
	private HBox menuHBox;
	
	@FXML
	private Button buttonDeviceWork;
	
	@FXML
	private Button buttonViewRecords;
	
	@FXML
	private Button buttonViewPatients;
	
	@FXML
	private Button buttonAdmin;
	
	public Integer userId;
	
//	public ObservableList<String> portList;
	
//	private SerialConnection con = new SerialConnection();

	
	@FXML
	public void initialize() {	

		
//		ObservableList<String> portList = con.detectPort();
//		portComboBox.getItems().addAll(portList);
//		portComboBox.valueProperty()
//               .addListener(new ChangeListener<String>() {

//            @Override
//            public void changed(ObservableValue<? extends String> observable, 
//                    String oldValue, String newValue) {
//                con.disconnectDevice();
//                con.connectDevice(newValue);
//            }

//        });	

	}

	@FXML
	private void handleTranslate(ActionEvent event) {
		TestConnection usertype = new TestConnection();
		int typeOfUser = usertype.getUserPermisions(labelUsername.getText().toString());
		System.out.println(labelUsername.getText().toString());
		System.out.println(typeOfUser);
	}
	
	@FXML
	public void onclickFincon(ActionEvent event) throws IOException
	{		
		griPaneMeniu.getChildren().clear();			
		FXMLLoader loader = new FXMLLoader(getClass().getResource("FinCon.fxml"));
        Parent finCon = loader.load();
        FinCon finConController = loader.getController();
        finConController.setUsername(labelUsername.getText());        
        griPaneMeniu.getChildren().add(finCon); 
	}
	
	@FXML
	public void onclickMuscon(ActionEvent event)
	{
		griPaneMeniu.getChildren().clear();
	}
	
	@FXML
	private void onclickDeviceWork(ActionEvent event){
		griPaneMeniu.getChildren().clear();
	}
	
	@FXML
	private void onclickViewRecords(ActionEvent event) throws IOException{
		griPaneMeniu.getChildren().clear();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("ViewStats.fxml"));
		Parent stats = loader.load();
		griPaneMeniu.getChildren().add(stats);
	}
	
	@FXML
	private void onclickViewPatients(ActionEvent event) throws IOException{
		griPaneMeniu.getChildren().clear();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("Patients.fxml"));
		Parent patient = loader.load();
		griPaneMeniu.getChildren().add(patient);
	}
	
	@FXML
	private void onclickAdmin(ActionEvent event) throws IOException{
		griPaneMeniu.getChildren().clear();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("Admin.fxml"));
		Parent admin = loader.load();
		griPaneMeniu.getChildren().add(admin);
	}
	
	@FXML
	public void setUsername(String username) {
		labelUsername.setText(username);
	}
	
	@FXML
	public void setPermisions(Integer permision) {
		// TODO We need to add a Demo level
		switch(permision) {
		case 0:		// Admin Level
			buttonDeviceWork.setVisible(true);
			buttonViewRecords.setVisible(true);
			buttonViewPatients.setVisible(false);
			buttonAdmin.setVisible(true);
			break;
		case 1:		// Doctor Level
			buttonDeviceWork.setVisible(true);
			buttonViewRecords.setVisible(true);
			buttonViewPatients.setVisible(true);
			buttonAdmin.setVisible(false);
			break;
		case 2:		// Patient level
			buttonDeviceWork.setVisible(true);
			buttonViewRecords.setVisible(true);
			buttonViewPatients.setVisible(false);
			buttonAdmin.setVisible(false);
			break;
		default:		// No level 
			buttonDeviceWork.setVisible(false);
			buttonViewRecords.setVisible(false);
			buttonViewPatients.setVisible(false);
			buttonAdmin.setVisible(false);
			break;
		}
	}
	
	@FXML
	public void setId(Integer id) {
		userId = id;
	}

}
