package application;

import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;

public class ViewStats {
	
	@FXML 
	GridPane gridPaneViewStats;
	
	@FXML
	private void initialize() {	
		gridPaneViewStats.setPrefWidth(1500);
		gridPaneViewStats.setPrefHeight(500);
	}

}
