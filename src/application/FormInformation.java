package application;

import java.sql.Date;
import java.sql.Time;

public class FormInformation {
	
	private String fileName = null;
	
	private String portName = null;
	
	private Integer numberOfExecises;
	
	private Integer sensorIndex = -1;
	
	private Integer chooseModel;
	
	private Integer stepNumber = 1;
	
	private Integer[][] stepDetail = new Integer[2][4];
	
	private Date thisDate;
	
	private Time thisTime;
	
	public void setDate(java.util.Date date) {
		thisDate = new Date(date.getTime());
		thisTime = new Time(date.getTime());
		}
	
	public Date getMyDate() {
		return thisDate;
	}

	public Time getMyTime() {
		return thisTime;
	}
	
	public void setStepDetails(Integer sensitivity, Integer length, Integer step) {
		stepDetail[0][step] = sensitivity;
		stepDetail[1][step] = length;
	}
	
	public Integer getStepLength(Integer step) {
		return stepDetail[1][step];
	}
	
	public Integer getStepSensitivity(Integer step) {
		return stepDetail[0][step];
	}
	
	public Integer[] getStepsLength() {
		return stepDetail[1];
	}

// ----------------- WTF is this??????----------------	
	public Integer[] getStepsSensitivyty() {
		return stepDetail[0];
	}
// ---------------------------------------------------
	
	public void setStepNumber(Integer number) {
		stepNumber = number;
	}
	
	public Integer getStepNumber() {
		return stepNumber;
	}
	
	public void setModelType(Integer index) {
		chooseModel = index;
	}
	
	public Integer getModelType() {
		return chooseModel;
	}
	
	public void setSensorIndex(Integer index) {
		sensorIndex = index;
	}
	
	public Integer getSensorIndex() {
		return sensorIndex;
	}
	
	public void setNoExercise(Integer number) {
		numberOfExecises = number;
	}
	
	public Integer getNoExercise()
	{
		return numberOfExecises;
	}
	
	public void setPortName(String port)
	{
		if(port.isEmpty())
		{
			portName = null;
		}
		else
		{
			portName = port;
		}
	}
	
	public String getPortName()
	{
		return portName;
	}
	
	public void setFileName(String fn) {
		if (fn.isEmpty())
		{
			fileName = null;
		}
		else
		{
			fileName = fn;			
		}	
	}
	
	public String getFileName()
	{
		return fileName;
	}

}
