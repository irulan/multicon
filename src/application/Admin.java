package application;

import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;

public class Admin {
	
	@FXML
	GridPane gridPaneAdmin;
	
	@FXML
	private void initialize() {	
		gridPaneAdmin.setPrefWidth(1500);
		gridPaneAdmin.setPrefHeight(500);
	}

}
