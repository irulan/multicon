package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortException;
import jssc.SerialPortList;


public class Main extends Application {
	
	private FinCon con;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("LogIn.fxml"));
			Scene scene = new Scene(root,300,275);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		primaryStage.setTitle("MultiCon");
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
    @Override   
    public void stop() throws Exception {
        con.finalize();
        super.stop();
    }
}
