package application;

import java.io.IOException;

import connection.TestConnection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.Node;
import javafx.stage.Stage;

public class LogIn{
	
	@FXML
	TextField usernameTextField;
	
	@FXML
	PasswordField passwdPasswdField;
	
	@FXML
	Button logInButton;
	
	@FXML
	public void handleSubmitButtonAction(ActionEvent event) throws IOException
	{
		String user = usernameTextField.getText().toString();
//		String pass = "";
		String pass = passwdPasswdField.getText().toString();
//		
		TestConnection test = new TestConnection();
		if(test.testLogIn(user, pass) == true) {
			
//			FXMLLoader finConLoader = new FXMLLoader(getClass().getResource("FinCon.fxml"));
//			Parent root = finConLoader.load();
//			FinCon finConController = finConLoader.getController();
//			finConController.setUsername(user);
			
			FXMLLoader menuLoader = new FXMLLoader(getClass().getResource("Menu.fxml"));
	        Parent root = menuLoader.load();
	        MenuFX menuController = menuLoader.getController();
	        menuController.setUsername(user);
	        
	        int permision = test.getUserPermisions(user);
	        int id = test.getUserId(user);
	        menuController.setPermisions(permision);
	        menuController.setId(id);
	        
	        Scene menuScene = new Scene(root,400,400);
	        //menuScene.setRoot(root);
	        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
	        window.setScene(menuScene);
	        window.setMaximized(true);
			window.show();

		}
		
	}

}
