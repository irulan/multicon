package application;

import static jssc.SerialPort.MASK_RXCHAR;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.FTPUploadFile;
import connection.TestConnection;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortException;
import jssc.SerialPortList;


public class FinCon {
	
	private Integer rowIndex = 0;
	
	private Integer colIndex = 0;
	
	private Label[] stepsLabels = new Label[4];
	
	private TextField[] stepsTextFieldsSensitivity = new TextField[4];
	
	private TextField[] stepsTextFieldsLen = new TextField[4];
	
	private Group[] groupSteps = new Group[4];
	
	private Button startButton = new Button("Start");
		
	private ObservableList<String> portList;
	
	public LineChart<Number,Number> lineChart;
	
    public SerialPort devicePort = null;
    
    private final int NUM_OF_POINT = 500;
    
    private final int NUM_OF_READING = 1000;
    
    private XYChart.Series<Number,Number> series;
    
    private XYChart.Series<Number, Number> nullSeries;
    
    private XYChart.Series<Number,Number> model;
    
    private FormInformation info = new FormInformation();
        
    private int counter = 0;
    
    private int localRepeats = 0;
    
    private File file = new File("out.txt");
    
    String user;
    
	@FXML
	private ComboBox<String> portComboBox;
	
	@FXML
	private Spinner<Integer> spinnerExNo;
	
	@FXML
	private ChoiceBox<String> choiceBoxModel;
	
	@FXML
	private Spinner<Integer> spinnerNumberOfSteps;
	
	@FXML
	private Label labelNumberOfSteps = new Label("4. Number of Steps");
	
	@FXML
	private GridPane gridPaneFinCon;
		
	@FXML
	private TextField fileNameTextField;
	
	@FXML Label labelUsername = new Label();

	
	@FXML
	private void initialize() {	
		
		detectPort();
		portComboBox.getItems().addAll(portList);		
		gridPaneFinCon.setPrefWidth(300);
		
		initSpinner();
      
		/* ---- Start Button handler  ----*/
		startButton.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) { 
        	  /* ---- Variables I need ----
        	   * 1. File name username + date + hour + minute + second
        	   * 2. Port
        	   * 3. Number of repeats
        	   * 4. Is model used
        	   * 5. If model is used how many steps
        	   * 6. If model is used step length and step sensitivity
        	   */   
// 1.---- Filename ----
        	  Calendar cal = Calendar.getInstance();
        	  Date mydate=cal.getTime();
        	  DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
        	  String formattedDate=dateFormat.format(mydate);       	
        	  String filename;

        	  filename=user+formattedDate;
        	  info.setFileName(filename);
        	  info.setDate(mydate);
// 2.---- Port Name ----
        	  info.setPortName(portComboBox.getSelectionModel().getSelectedItem().toString());
// 3.---- Number of exercices/repeats ----
        	  info.setNoExercise(spinnerExNo.getValue());
// 4.---- Chose model ----
        	  info.setModelType(choiceBoxModel.getSelectionModel().getSelectedIndex());  
        	  int modelType = info.getModelType();
        	  if(modelType == 0) {
// 5.---- Number of model steps ----
        		  info.setStepNumber(spinnerNumberOfSteps.getValue());
// 6.---- Step length and sensitivity loop ----        		  
				for(int i = 0; i < info.getStepNumber(); i++) {
					int sensitivity = Integer.parseInt(stepsTextFieldsSensitivity[i].getText());
					int length = Integer.parseInt(stepsTextFieldsLen[i].getText());
					info.setStepDetails(sensitivity, length, i);
				}
        	  }
        	  //* ---- Create file with information from point 1 to 6 ----
        	  try {
        		  BufferedWriter out;
             	  FileWriter fstream;
         		  fstream = new FileWriter(file);
             	  out = new BufferedWriter(fstream);
        		  out.write(info.getNoExercise().toString());
        		  out.write(" ");
        		  out.write(info.getModelType().toString());
        		  out.newLine();
        		  
        		  if(modelType == 0) {
        			  out.write(info.getStepNumber().toString());   
            		  out.newLine();
            		  for(int i = 0; i < info.getStepNumber(); i++) {
            			  int sensitivity = Integer.parseInt(stepsTextFieldsSensitivity[i].getText());
      					  int length = Integer.parseInt(stepsTextFieldsLen[i].getText());
            			  out.write(sensitivity + " " + length);   
	            		  out.newLine();
            		  }
        		  }
        		  
        		  out.close();
        		  fstream.close();
        		  
        	  }catch(Exception e) {
         		 System.err.println("Error: " + e.getMessage());
         	  }         	  
        	  
        	  finConChart();
              disconnectDevice();
              connectDevice(info.getPortName());

          }
      });
	}
	
	@FXML
	private void choiceBoxController() {
		rowIndex = GridPane.getRowIndex(choiceBoxModel);
		
		String select = choiceBoxModel.getValue().toString();
		
		switch(select)
		{
		case "Yes":
			final int initialValue = 0;
			final int firstValue = 0;
			final int lastValue = 4;
			
			spinnerNumberOfSteps = new Spinner<Integer>();
			SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(firstValue, lastValue, initialValue);
			spinnerNumberOfSteps.setValueFactory(valueFactory);

			gridPaneFinCon.add(labelNumberOfSteps, colIndex, ++rowIndex);
			gridPaneFinCon.add(spinnerNumberOfSteps, colIndex, ++rowIndex);

			spinnerNumberOfSteps.valueProperty().addListener((obs, oldvalue, newvalue) ->
			{
				int value = (Integer) spinnerNumberOfSteps.getValue();
				
				for(int i = 0; i < oldvalue; i++){
					gridPaneFinCon.getChildren().remove(groupSteps[i]);
					rowIndex--;
				}
				
				for(int i = 0; i < value; i++) {
					Rectangle rect = new Rectangle();			
					rect.setArcHeight(5);
					rect.setArcWidth(5);
					rect.setHeight(25);
					rect.setWidth(124);
					rect.setFill(Color.web("#fefefe"));
					rect.setLayoutX(-3.0);
					rect.setStroke(Color.web("#959595"));
					rect.setStrokeType(StrokeType.INSIDE);
					rect.setStyle("-fx-border-radius: 5 5 5 5;" + "-fx-background-radius: 3 3 3 3 ;");
					
					int index = i + 1;
					stepsLabels[i] = new Label("Step" + index + ": ");
					stepsLabels[i].setLayoutX(2.0);
					stepsLabels[i].setLayoutY(3.0);
					
					stepsTextFieldsSensitivity[i] = new TextField("Sensitivity (gF)");
					stepsTextFieldsSensitivity[i].setLayoutX(49);
					stepsTextFieldsSensitivity[i].prefHeight(25);
					stepsTextFieldsSensitivity[i].prefWidth(47);
					stepsTextFieldsSensitivity[i].setStyle("-fx-border-radius: 0 0 0 0;" + "-fx-background-radius: 0 0 0 0 ;" + "-fx-border-width: 0px ;");
					
					stepsTextFieldsLen[i] = new TextField("Step Length (s)");
					stepsTextFieldsLen[i].setLayoutX(95);
					stepsTextFieldsLen[i].prefHeight(25);
					stepsTextFieldsLen[i].prefWidth(47);
					stepsTextFieldsLen[i].setStyle("-fx-border-radius: 0 3 3 0;" + "-fx-background-radius: 0 3 3 0 ;" + "-fx-border-width: 0px ;");
					
					groupSteps[i] = new Group();					
					groupSteps[i].getChildren().addAll(rect,stepsLabels[i],stepsTextFieldsSensitivity[i], stepsTextFieldsLen[i]);
					
					gridPaneFinCon.add(groupSteps[i], colIndex, ++rowIndex); 
				}
				
			}
			);			
			break;
			
		case "No":
			System.out.println("No");
			break;
		default:
			break;
		}

		gridPaneFinCon.add(startButton, colIndex, ++rowIndex);

	}
	
	private void finConChart()
	{
/*
 * 1. Set grid dimensions
 * 2. Set grid axis
 * 3. Create model
 * 4. Reset series
 */
		
// 1. ---- Set grid dimensions ----
		gridPaneFinCon.setPrefHeight(800);
		gridPaneFinCon.setPrefWidth(1500);
	    gridPaneFinCon.getChildren().clear();

// 2. ---- Set grid axis ----
		
	    final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Force");
        yAxis.setAutoRanging(false);
        yAxis.setUpperBound(100);
        
        xAxis.setLabel("Time (ms)");
        xAxis.setAutoRanging(false);
        xAxis.setUpperBound(550);
        xAxis.setTickUnit(50);
        
        
        lineChart = new LineChart<>(xAxis,yAxis);
        lineChart.getStyleClass().add("thick-chart");
        lineChart.setTitle("Analog Input");

// 3. ---- Create model ----
        series = new XYChart.Series<Number,Number>();
        int numberOfSteps = info.getStepNumber();
        if(info.getModelType() == 0)
        {
        	model = new XYChart.Series<Number,Number>();
        	int totallen = 0;
        	int maxSen = 0;
        	for(int i = 0; i < numberOfSteps; i++)
        	{
        		int len = info.getStepLength(i)*10;
        		int sen = info.getStepSensitivity(i);
        		
        		if(sen > maxSen)
        			maxSen = sen;
        		
        		model.getData().add(new XYChart.Data<Number, Number>(totallen, sen));
        		model.getData().add(new XYChart.Data<Number, Number>(len + totallen, sen));
//                lineChart.getData().add(model);
                
                totallen += len;
                
        	}
        	model.getData().add(new XYChart.Data<Number, Number>(totallen+0.1, 0));
        	lineChart.getData().add(model);
        	yAxis.setUpperBound(maxSen * 3);
            xAxis.setUpperBound(totallen * 1.2);
        }

// 4. ---- Reset series ----        
        nullSeries = new XYChart.Series<Number,Number>();
        series.setName("A0 analog input");
        lineChart.getData().add(series);
        
        lineChart.setAnimated(false);
        lineChart.setCreateSymbols(false);
	
        
//pre-load nullSeries with 0
        for(int i=0; i<NUM_OF_READING/100; i++){
        	nullSeries.getData().add(new XYChart.Data<Number,Number>(i, 0));
        }
        
		gridPaneFinCon.add(lineChart, 0, 0);
	    GridPane.setFillWidth(lineChart, true);
	    GridPane.setFillHeight(lineChart, true);
	    gridPaneFinCon.setAlignment(Pos.CENTER);
	}
	
	private void initSpinner() {
		spinnerExNo.setValueFactory(
	            new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 10));
	}
	
    private void detectPort(){
        
        portList = FXCollections.observableArrayList();
 
        String[] serialPortNames = SerialPortList.getPortNames();
        for(String name: serialPortNames){
            System.out.println(name);
            portList.add(name);
        }
    }

   private void disconnectDevice(){
        
        System.out.println("disconnectDevice()");
        if(devicePort != null){
            try {
                devicePort.removeEventListener();
                
                if(devicePort.isOpened()){
                    devicePort.closePort();
                }
                
            } catch (SerialPortException ex) {
                Logger.getLogger(Main.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
    }

    private boolean connectDevice(String port){
        
        System.out.println("connectDevice");
        
        boolean success = false;
        SerialPort serialPort = new SerialPort(port);
        try {
            serialPort.openPort();
            serialPort.setParams(
                    SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            serialPort.setEventsMask(MASK_RXCHAR);
            serialPort.addEventListener((SerialPortEvent serialPortEvent) -> {
                if(serialPortEvent.isRXCHAR()){
                    try {
                    	String st = serialPort.readString();
                        String[] parsest = st.split("\\r?\\n");
                        if(parsest.length > 0)
                        {
                        	int value = Integer.parseInt(parsest[0]);
                        	//Update label in ui thread
                            Platform.runLater(() -> {
                                shiftSeriesData((float)value); //in 5V scale
                            });
                        }
                        
                        
                    } catch (SerialPortException ex) {
                        Logger.getLogger(Main.class.getName())
                                .log(Level.SEVERE, null, ex);
                    }
                    
                }
            });
            
            devicePort = serialPort;
            success = true;
        } catch (SerialPortException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
            System.out.println("SerialPortException: " + ex.toString());
        }

        return success;
    }

    private void shiftSeriesData(float newValue)
    {
    	int repeats = info.getNoExercise();
    	int length = series.getData().size();
    	
// ---- If there are no repeats to do stop exercise ----
    	if(repeats == 0)
    		disconnectDevice();
  
// ---- Counting the number of reads that the sensor reads "0", if there are more than "10" you reset series ----
    	if(newValue > 0)
    	{
    		counter = 0;
    	}
    	else
    	{
    		counter++;
    	}

    	if(counter == 10)
    	{
    		if(length > 40)
    		{
    			localRepeats++;
    			System.out.println(localRepeats+ "/"+repeats);
    			if(localRepeats == repeats)
    			{
    				disconnectDevice();	
    				try {
    					String current = new java.io.File( "." ).getCanonicalPath();
        		        String localPath = current + "/out.txt";
        		        String remotePath = info.getFileName();
        		        FTPUploadFile.getFTPConnection(localPath, remotePath);
        		        TestConnection.assignFileToUser(remotePath, user, info.getMyDate(), info.getMyTime());
    				}catch(Exception e) {
    					
    				}
    				
    			}
    		}
    			
    		series.getData().clear();
    		counter = 0;
    		length = 0;
        	  try {
        		  BufferedWriter out;
             	  FileWriter fstream;
         		  fstream = new FileWriter(file, true);
             	  out = new BufferedWriter(fstream);
             	  out.newLine();
        		  
        		  out.close();
        		  fstream.close();
        		  
        	  }catch(Exception e) {
         		 System.err.println("Error: " + e.getMessage());
         	  } 
    	}
    	else
    	{
      	  try {
    		  BufferedWriter out;
         	  FileWriter fstream;
     		  fstream = new FileWriter(file, true);
         	  out = new BufferedWriter(fstream);
    		  out.write(String.valueOf(newValue) + " ");
    		  
    		  out.close();
    		  fstream.close();
    		  
    	  }catch(Exception e) {
     		 System.err.println("Error: " + e.getMessage());
     	  }         	  
			
    		if(length < NUM_OF_POINT)
        	{
        		series.getData().add(new XYChart.Data<Number,Number>(length, newValue));
        	}
        	else
        	{
                for(int i=0; i<NUM_OF_POINT-1; i++){
                    XYChart.Data<Number, Number> ShiftDataUp = 
                            (XYChart.Data<Number, Number>)series.getData().get(i+1);
                    Number shiftValue = ShiftDataUp.getYValue();
                    XYChart.Data<Number, Number> ShiftDataDn = 
                            (XYChart.Data<Number, Number>)series.getData().get(i);
                    ShiftDataDn.setYValue(shiftValue);
                }
                XYChart.Data<Number, Number> lastData = 
                    (XYChart.Data<Number, Number>)series.getData().get(NUM_OF_POINT-1);
                lastData.setYValue(newValue);
            }

    	}
    	
    }
    
    @FXML
	public void setUsername(String username) {
    	user = username;
		labelUsername.setText(user);
	}
    
    public void finalize() {
    	disconnectDevice();
    }
}
