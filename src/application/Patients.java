package application;

import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;

public class Patients {

	@FXML
	GridPane gridPaneViewPatients;
	
	@FXML
	private void initialize() {	
		gridPaneViewPatients.setPrefWidth(1500);
		gridPaneViewPatients.setPrefHeight(500);
	}
}
